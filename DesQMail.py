#!/usr/bin/python3

"""
DesQMail - The main application

###================ Program Info ================###
    Module Name : DesQMail
    Version : 1.0.0
    Platform : Linux/Unix
    Requriements :
        Must :
            modules os, sys, PyQt5, QWebEngine
        Optional :
            module readline
    Python Version : Python 3.7 or higher
    Author : Britanicus
    Email : marcusbritanicus@gmail.com
    License : GPL version 3
###==============================================###
"""

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

    #
    # Copyright 2020 Britanicus <marcusbritanicus@gmail.com>
    #

    #
    # This program is free software; you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation; either version 2 of the License, or
    # ( at your option ) any later version.
    #

    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.
    #

    #
    # You should have received a copy of the GNU General Public License
    # along with this program; if not, write to the Free Software
    # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    # MA 02110-1301, USA.
    #

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

import sys
from PyQt5.QtWidgets import QApplication

from UI.UI import DesQMailUI

app = QApplication( sys.argv )

Gui = DesQMailUI()
Gui.showMaximized()

app.exec_()
