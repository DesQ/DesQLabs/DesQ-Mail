#!/usr/bin/python3

"""
UI - Contains the DesQ Mail UI

###================ Program Info ================###
    Module Name : UI
    Version : 1.0.0
    Platform : Linux/Unix
    Requriements :
        Must :
            modules os, sys, PyQt5, QWebEngine
        Optional :
            module readline
    Python Version : Python 3.7 or higher
    Author : Britanicus
    Email : marcusbritanicus@gmail.com
    License : GPL version 3
###==============================================###
"""

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

    #
    # Copyright 2020 Britanicus <marcusbritanicus@gmail.com>
    #

    #
    # This program is free software; you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation; either version 2 of the License, or
    # ( at your option ) any later version.
    #

    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.
    #

    #
    # You should have received a copy of the GNU General Public License
    # along with this program; if not, write to the Free Software
    # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    # MA 02110-1301, USA.
    #

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

import os, sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import email

from Engines.MailEngine import PostalService

from UI.EnvelopesView import EnvelopesView
from UI.MailView import MailView

class DesQMailUI( QMainWindow ):
    """A smooth modern interface for IMAP/SMTP eMail providers
    """

    def __init__( self ) :
        """Class initializer
        """

        super( DesQMailUI, self ).__init__()
        self.createUI()
        self.setupConnections()

        self.mailBox = "/tmp/mailbox/"

        uname = QInputDialog.getText(
            self,
            "DesQ Mail | GMail Username",
            "Enter your gmail username:",
            QLineEdit.Normal
        )

        paswd = QInputDialog.getText(
            self,
            "DesQ Mail | GMail Password",
            "Enter your gmail password:",
            QLineEdit.Password
        )

        if not ( uname[ 1 ] and paswd[ 1 ] ):
            return

        self.mailEngine = PostalService( "imap.gmail.com", 993, uname[ 0 ], paswd[ 0 ], self.mailBox )
        self.mailEngine.login()

    def createUI( self ) :
            """createUI() -> None

            Create the user interface

            @return None
            """

            shade = self.palette().color( QPalette.Window ).darker( 110 )

            # Shows the mailboxes
            self.trays = QWidget()
            self.trays.setStyleSheet( "background-color: %s; border: none; border-radius: 5px; padding 5px;" % shade.name() )

            self.trays.setFixedWidth( 36 )
            self.trayLyt = QVBoxLayout()

            # Mail Envelopes
            self.envelopes = EnvelopesView()
            self.envelopes.setFixedWidth( 270 )

            # Mail tools
            self.toolBar = QWidget()
            self.toolBar.setFixedHeight( 24 )

            # Mail tools
            self.header = QLabel()
            clr = self.palette().color( QPalette.Window ).darker( 110 )
            self.header.setStyleSheet( "background-color: %s; border: none; border-radius: 5px; padding 10px;" % shade.name() )
            self.header.setText( "<b><large><font size = 5>&nbsp;Test mail</font></large></b>" )
            self.header.setFixedHeight( 36 )

            # Selected email
            self.mailView = MailView()
            self.mailView.setHtml( "Dear Marcus,<br><br>This is a test mail sent from <a href='protonmail.com'>protonmail.com</a>.<br><br>Regards,<br>Marcus<br>" )

            # Attachments area
            self.annex = QScrollArea()
            self.annex.setWidgetResizable( True )
            self.annex.setHorizontalScrollBarPolicy( Qt.ScrollBarAlwaysOff )
            self.annex.setFixedHeight( 96 )
            self.annex.setStyleSheet( "background-color: %s; border: none; border-radius: 5px; padding 5px;" % shade.name() )

            base = QWidget()
            baseLyt = QGridLayout()
            baseLyt.setContentsMargins( QMargins( 5, 5, 5, 5 ) )
            baseLyt.setSpacing( 5 )
            baseLyt.addWidget( self.trays,     0, 0, 4, 1 )
            baseLyt.addWidget( self.envelopes, 0, 1, 4, 1 )
            baseLyt.addWidget( self.toolBar,   0, 2, 1, 1 )
            baseLyt.addWidget( self.header,    1, 2, 1, 1 )
            baseLyt.addWidget( self.mailView,  2, 2, 1, 1 )
            baseLyt.addWidget( self.annex,     3, 2, 1, 1 )
            base.setLayout( baseLyt )

            self.setCentralWidget( base )

            # Window Properties
            self.setMinimumSize( QSize( 800, 600 ) )
            self.setWindowTitle( "DesQ Mail" )
            self.setWindowIcon( QIcon.fromTheme( "geary" ) )

    def setupConnections( self ):

        self.envelopes.clicked[ QModelIndex ].connect( self.fetchMail )

    def fetchMail( self, index ):

        if not os.path.exists( f"{index.data()}.eml" ):
            data = index.data().replace( self.mailBox, "" )
            uid = int( os.path.basename( data ) )
            mailbox = data.replace( f"/{uid}", "" )

            self.mailEngine.fetch( [ [ mailbox, ( uid, ) ] ] )

        subject = email.message_from_bytes( open( f"{index.data()}.eml", 'rb' ).read() )[ 'Subject' ]
        self.header.setText( f"<b><large><font size = 5>&nbsp;{subject}</font></large></b>" )
        self.mailView.loadMail( f"{index.data()}.eml" )

if __name__ == '__main__' :

    app = QApplication( sys.argv )

    Gui = DesQMailUI()
    Gui.show()

    sys.exit( app.exec_() )
