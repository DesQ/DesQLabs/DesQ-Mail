#!/usr/bin/python3

"""
EnvelopesView - ListView subclass to show the eMail Envelopes

###================ Program Info ================###
    Program Name : EnvelopesView
    Version : 1.0.0
    Platform : Linux/Unix
    Requriements :
        Must :
            modules os, sys, PyQt5
        Optional :
            module readline
    Python Version : Python 3.7 or higher
    Author : Britanicus
    Email : marcusbritanicus@gmail.com
    License : GPL version 3
###==============================================###
"""

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

    #
    # Copyright 2020 Britanicus <marcusbritanicus@gmail.com>
    #

    #
    # This program is free software; you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation; either version 2 of the License, or
    # ( at your option ) any later version.
    #

    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.
    #

    #
    # You should have received a copy of the GNU General Public License
    # along with this program; if not, write to the Free Software
    # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    # MA 02110-1301, USA.
    #

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

import os, sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class EnvelopesModel( QStringListModel ):
    """Model for loading Mail Envelopes."""

    def __init__( self, maildir, mailbox ):
        """
        Initializer
        """
        super( EnvelopesModel, self ).__init__()

        # Folder where all mails of this email-ID are stored
        self.mailDir = maildir[:]
        self.mailBox = mailbox[:]

        self.mbox = QDir( f"{self.mailDir}{self.mailBox}/" )
        self.mboxInfo = QSettings( self.mbox.filePath( ".mboxinfo" ), QSettings.IniFormat )
        self.updateModel()

        self.updateTimer = QBasicTimer()
        self.updateTimer.start( 1000, self )

    def changeMailBox( self, mboxName ):

        self.mbox = QDir( f"{self.mailDir}{self.mailBox}/" )
        self.mboxInfo = QSettings( self.mbox.filePath( ".mboxinfo" ), QSettings.IniFormat )
        self.updateModel()

    def timerEvent( self, tEvent ):
        """
        Update the model
        """

        if self.updateTimer.timerId() == tEvent.timerId():
            self.updateModel()

        else:
            QStringListModel.timerEvent( self, tEvent )

    def updateModel( self ):

        self.mboxInfo.sync()

        if not self.mboxInfo.contains( "UIDs" ):
            return

        envelopes = [ self.mbox.filePath( f"{uid}" ) for uid in self.mboxInfo.value( "UIDs" ) ]
        self.setStringList( envelopes )

    def info( self, uid ):

        self.mboxInfo.sync()
        uidInfo = {
            "From": self.mboxInfo.value( f"{uid}/From" ),
            "Subject": self.mboxInfo.value( f"{uid}/Subject" ),
            "Date": self.mboxInfo.value( f"{uid}/Date" ),
            "CC": self.mboxInfo.value( f"{uid}/CC" ),
            "BCC": self.mboxInfo.value( f"{uid}/BCC" )
        }

        return uidInfo

class EnvelopesView( QListView ):
    """A smooth modern interface for IMAP/SMTP eMail providers
    """

    def __init__( self ) :
        """Class initializer
        """

        super( EnvelopesView, self ).__init__()
        self.setFrameStyle( QFrame.NoFrame )

        self.setViewMode( QListView.ListMode )
        self.setMovement( QListView.Static )
        self.setResizeMode( QListView.Adjust )
        self.setGridSize( QSize( 270, 70 ) )
        self.setSelectionMode( QListView.SingleSelection )

        clr = self.palette().color( QPalette.Window ).darker( 110 )
        self.setStyleSheet( "background-color: %s; border: none; border-radius: 5px; padding 5px;" % clr.name() )

        self.setVerticalScrollBarPolicy( Qt.ScrollBarAlwaysOff )

        self.setModel( EnvelopesModel( "/tmp/mailbox/", "INBOX" ) )
        self.setItemDelegate( EnvelopeRenderer() )

class EnvelopeRenderer( QStyledItemDelegate ):

    def __init__( self, parent = None ):

        super( EnvelopeRenderer, self ).__init__( parent )

        self.fFM = QFontMetrics( QFont( qApp.font().family(), qApp.font().pointSize() + 1, QFont.Bold ) )
        self.sFM = QFontMetrics( QFont( qApp.font().family(), qApp.font().pointSize() ) )

        self.doc = QTextDocument()
        self.doc.setTextWidth( 250 )
        self.doc.setDocumentMargin( 0 )

        textOpts = self.doc.defaultTextOption()
        textOpts.setWrapMode( QTextOption.NoWrap )
        self.doc.setDefaultTextOption( textOpts )

    def paint( self, painter, option, index ):

        rect = option.rect

        X = rect.left()
        Y = rect.top()

        painter.save()
        painter.setRenderHints( QPainter.Antialiasing )

        data = index.data( Qt.DisplayRole )
        model = index.model()

        info = model.info( os.path.basename( data ) )

        sender = self.fFM.elidedText( info[ "From" ], Qt.ElideRight, rect.width() - 20 )
        subject = self.sFM.elidedText( info[ "Subject" ], Qt.ElideRight, rect.width() - 20 )

        date = QDateTime.fromString( info[ 'Date' ] ).toString( "MMM dd, yyyy" )
        cc_bcc = "CC" if info[ "CC" ] else "-" + "BCC" if info[ "BCC" ] else ""
        cc_bcc = cc_bcc[ 1: ] if cc_bcc.startswith( '-' ) else cc_bcc

        if option.state & QStyle.State_MouseOver :
            painter.save()
            painter.setPen( Qt.NoPen )

            clr = option.palette.color( QPalette.Highlight )
            clr.setAlpha( 27 )
            painter.setBrush( clr )
            painter.drawRoundedRect( QRectF( X + 5, Y + 5, rect.width() - 10, rect.height() - 5 ), 5, 5 )
            painter.restore()

        elif option.state & QStyle.State_Selected :
            painter.save()
            painter.setPen( Qt.NoPen )
            clr = option.palette.color( QPalette.Highlight )
            clr.setAlpha( 27 )
            painter.setBrush( QColor( 0, 0, 0, 54 ) )
            painter.drawRoundedRect( QRectF( X + 5, Y + 5, rect.width() - 10, rect.height() - 5 ), 5, 5 )
            painter.restore()

        else :
            painter.save()
            painter.setPen( Qt.NoPen )
            painter.setBrush( QColor( 0, 0, 0, 54 ) )
            painter.drawRoundedRect( QRectF( X + 5, Y + 5, rect.width() - 10, rect.height() - 5 ), 5, 5 )
            painter.restore()

        font = painter.font()

        self.doc.setHtml( f"""
            <table style = "font-family: Quicksand;" width = 100% border = 0>
                <tr>
                    <td style = "text-align: left; font-size: {font.pointSize() + 1}pt;" colspan = 2><b>{sender}</b></td>
                </tr>
                <tr>
                    <td style = "text-align: left; font-size: {font.pointSize()}pt;" colspan = 2>{subject}</td>
                </tr>
                <tr>
                    <td style = "text-align: left; font-size: {font.pointSize() - 1}pt; color: #555555;">{cc_bcc}</td>
                    <td style = "text-align: right; font-size: {font.pointSize() - 1}pt;">{date}</td>
                <tr>
            </table>
        """ )
        painter.translate( QPoint( X + 10, Y + 10 ) )
        self.doc.drawContents( painter, QRectF( 0, 0, rect.width() - 20, rect.height() - 10 ) )

        painter.restore()

    def sizeHint( self, option, index ):

        return QSize( 270, 70 );

if __name__ == '__main__' :

    app = QApplication( sys.argv )

    Gui = EnvelopesView()
    Gui.show()

    sys.exit( app.exec_() )
